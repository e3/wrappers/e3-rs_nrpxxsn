require stream
require rs_nrpxxsn
require autosave
require iocstats

epicsEnvSet("TOP",     "$(E3_CMD_TOP)/..")
epicsEnvSet("AS_TOP",  "$(E3_CMD_TOP)")
epicsEnvSet("IOCDIR",  "TS2-010:Ctrl-IOC-002")
epicsEnvSet("IOCNAME", "TS2-010RFC:SC-IOC-007")
epicsEnvSet("SETTINGS_FILES","settings")

# osp230
epicsEnvSet("P",	"TS2-010CRM:")
epicsEnvSet("CAL",	"EMR-RFPM-CAL:")
epicsEnvSet("OSP",	"EMR-OSP-010:")
# pr-01-srf
epicsEnvSet("N1",	"1")
epicsEnvSet("R1",	"EMR-RFPM-010:")
epicsEnvSet("IP1",	"pr-01-srf.tn.esss.lu.se")
# pr-02-srf
epicsEnvSet("N2",	"2")
epicsEnvSet("R2",	"EMR-RFPM-020:")
epicsEnvSet("IP2",	"pr-02-srf.tn.esss.lu.se")
# pr-03-srf
epicsEnvSet("N3",	"3")
epicsEnvSet("R3",	"EMR-RFPM-030:")
epicsEnvSet("IP3",	"pr-03-srf.tn.esss.lu.se")
# pr-04-srf
epicsEnvSet("N4",	"4")
epicsEnvSet("R4",	"EMR-RFPM-040:")
epicsEnvSet("IP4",	"pr-04-srf.tn.esss.lu.se")

# Add extra startup scripts requirements here
iocshLoad("$(autosave_DIR)/autosave.iocsh")
iocshLoad("$(iocstats_DIR)/iocStats.iocsh")
iocshLoad("${rs_nrpxxsn_DIR}/rs_nrpxxsn.iocsh", "P=$(P), R=$(R1), N=$(N1), IP=$(IP1), ASYN_PORT=$(R1)")
iocshLoad("${rs_nrpxxsn_DIR}/rs_nrpxxsn.iocsh", "P=$(P), R=$(R2), N=$(N2), IP=$(IP2), ASYN_PORT=$(R2)")
iocshLoad("${rs_nrpxxsn_DIR}/rs_nrpxxsn.iocsh", "P=$(P), R=$(R3), N=$(N3), IP=$(IP3), ASYN_PORT=$(R3)")
iocshLoad("${rs_nrpxxsn_DIR}/rs_nrpxxsn.iocsh", "P=$(P), R=$(R4), N=$(N4), IP=$(IP4), ASYN_PORT=$(R4)")
dbLoadRecords("calibration.db", "P=$(P), CAL=$(CAL), R1=$(R1), R2=$(R2), R3=$(R3), R4=$(R4), OSP=$(OSP)")

# Call autosave function
afterInit("fdbrestore("$(AS_TOP)/$(IOCNAME)/save/$(SETTINGS_FILES).sav")")

# Start any sequence programs
seq("sncProcess","P=$(P), R=$(R1)")
seq("sncProcess","P=$(P), R=$(R2)")
seq("sncProcess","P=$(P), R=$(R3)")
seq("sncProcess","P=$(P), R=$(R4)")

# Call iocInit to start the IOC
iocInit()
